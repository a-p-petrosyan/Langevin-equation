#+TITLE: Research proposal to university of Milan
#+AUTHOR: Aleksandr Petrosyan
#+OPTIONS: h:1 num:nil toc:nil

Relativistic statistical mechanics is a vast area of physics with
relatively unexplored potential.  It is well-known that the early
universe was not only compact, but also extremely hot. For a long
period of time, the universe was filled with a super-heated plasma,
whose behaviour no longer required a full QFT treatment to be
described accurately, but which still had considerable relativistic
corrections to contend with. There had been relatively few quite
focused studies into statistical mechanics in this energy range, a
void which I intend to fill.

* What I intend to research.
  
  This PhD project will focus on problems of relativistic statistical
  mechanics, in particular the problems of nonequilibrium dynamics in a
  regime where the energies are sufficient for relativistic
  corrections to be significant, but not sufficient for pair
  production to play a significant role (Langevin equation,
  relativistic Fokker-Planck) with applications to Quantum information
  theory, (e.g. a theory of relativistic quantum decoherence). 

  I intend to establish the order of corrections needed for a fully
  relativistic treatment of familiar physical systems. This area is
  also quite intriguing, because the Fokker Planck formalism is able
  to produce multiple long-distance correlations and EPR-style
  potentially super-luminal propagations of correlations without
  relativistic treatment. It is of great interest to modern cosmology,
  if these correlations could explain the observations from e.g. the
  Planck mission, instead of the assumption of cosmic inflation. 

  A relativistic treatment of super-heated plasmas is important for
  fusion energetics, as having a more accurate model of the behaviour
  of a plasma in the reactor and its statistics can lead to the
  production of the long overdue economically viable fusion
  reactor. Another potentially fruitful application is to physics of
  stellar evolution. Modern models of the development of stars have a
  few considerable omissions, in particular while one has a rather
  accurate and precise understanding of the causes of supernova
  explosions, modern astrophysics has yet to develop a precise
  theoretical model for the probability of supernova event near the
  end of a star's lifespan. Some of these problems may be easier to
  solve provided one has a relativistic theory of Brownian motion,
  diffusion and fluctuation-dissipation.

* Planned research

  The first preliminary step is the development of a fully Lorentz
  covariant Langevin equation. A promising candidate for such an
  equation was already derived under Prof. Zaccone's supervision by
  me.  The initial steps of the project would be to follow-up the
  corresponding publication:

** Derive the corresponding Fokker-Planck equation

   The initial problem with the Fokker Planck formalism is that it is
   inherently dealing in probability densities that can have apparent
   super-luminal propagation. While the relativistic analogue of the
   Fokker Planck equation should be possible to derive from the
   relativistic Langevin equation analogue, there are multiple hurdles
   that make the derivation far more complex than one could fit into a
   single publication. 

** Derive the corresponding fluctuation-dissipation relation

   The fluctuation dissipation theorem is one of the foundational
   results of modern statistical mechanics. It can be derived and
   proved fully from the Langevin equation, in the Newtonian
   mechanics. Relativistic treatment leads to a more complex Langevin
   equation from which the standard methods do not easily lead to a
   result that would be considered the relativistic dual of the
   fluctuation-dissipation theorem. 

** Following up the observational implications of the Langevin equation

   The derived Langevin equation has multiple implications for
   observational cosmology. In particular it predicts a restoring
   force with a frequency dependent on unknowable initial
   conditions. It is highly likely that the effective restoring force
   does not manifest in real systems. It is also possible that it may
   manifest as oscillations of a frequency that cannot be confused
   with any other physical process. Or it could contribute very close
   to the frequency of known processes: Baryon acoustic oscillations,
   Sakharov Oscillations, etc.  

** Adding pair production and elements of QFT

   While the derived Langevin equation is adequate for an intermediate
   energy regime, it may be worthwhile exploring an extension that
   could account in some capacity for high energy effects, such as
   pair production, and nuclear fusion just to name few. A
   field-theoretic description of Langevin processes may also be
   necessary for deriving the Fokker Planck equation mentioned
   earlier. 

** Building a theory of relativistic quantum decoherence
   
   The theory of evolution of quantum density matrices is one of the
   most important aspects of modern quantum information
   theory. Expanding upon the relativistic Langevin equation, it may
   be worth exploring what the analogue of the master decoherence
   equation looks like in Lorentz-covariant form. 

* Plan of execution
  Each of the aforementioned topics lends itself neatly to
  publications. Accomplishing either one or all of them may lead to
  several impactful publications. According to the plan presented
  here, the work is largely composed of smaller independent
  theoretical endeavours. The research project is flexible in terms of
  its focus. Different aspects can be prioritised depending on the
  circumstances, and the priorities of the research group. Finally,
  the redundancy serves as a fail-safe, in case any of the proposed
  research areas produce unexpected results. 


  The proposed work is multidisciplinary. The proposed research falls
  neatly into the category of statistical mechanics, but has profound
  implications for plasma physics, cosmology, and to a small extent,
  may be useful for Quantum information theory. Overall, I hope that
  the admissions committee finds my research proposal suitable for the
  PhD programme. 

  

  
