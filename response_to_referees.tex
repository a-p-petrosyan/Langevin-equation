\documentclass[prl,aps,onecolumn,groupaddress]{revtex4-1}
\usepackage{latexsym}
\usepackage{verbatim}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{mathrsfs}
\usepackage{float}
\usepackage{framed}
\usepackage{comment}
\usepackage[usenames, dvipsnames]{color}
\usepackage{mathtools}
\usepackage{slashed}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{bbold}
\usepackage{wasysym}

% \usepackage{natbib}
% \bibliographystyle{abbrvnat}
% \setcitestyle{authoryear,round} %Citation-related commands

\newenvironment{letterstyle}{%
  \setlength{\parindent}{0pt}%
  \setlength{\parskip}{1.5em}%
}{%
  \setlength\parindent{3em}%
  \setlength{\parskip}{0em}%
}%

\newenvironment{Referee}{%
  \noindent\begin{leftbar}\begin{quotation}\begin{letterstyle}%
}{%
  \end{letterstyle}\end{quotation}\end{leftbar}%
}%

\newenvironment{response}{%

  \par%
  \noindent\textbf{Response:}

  \begin{letterstyle}
}{%
  \end{letterstyle}
  \par%
}

\newenvironment{manuscript}{%

  \par\color{blue}%
  \noindent\textbf{Changes in the manuscript:}

  \begin{letterstyle}
}{%
  \end{letterstyle}
  \par%
}

\newcommand{\todo}{ \textbf{TODO} }

\begin{document}
\begin{letterstyle}
Dear Editor,

We would like to thank you for the detailed and very useful
comments from the Referees.
Both Referees expressed a positive evaluation of our manuscript and their criticism is mainly focused on presentation issues in the previous version of the manuscript and on additional validation of the approximations used, while both agree on the relevance and originality of our approach.

The major suggestions made by the Referees are as follows: (i) include a quantitative validation
of the result into the paper, rather than leave it attached to the
code accompanying the submission, as well as (ii) provide a discussion of
the role of determinism in the obtained result.

Both are valid suggestions that we implemented in the revised version.  In particular, in order to address (i) we
have carried out an extensive Bayesian validation of the analytical approximation
as compared to the numerical solution and we have added this analysis in the revised manuscript.

The second aspect (ii) is somewhat more delicate on a conceptual level, although it was already clarified in the seminal work of Zwanzig. It is true that the particle-bath coupling model
that we have chosen can only appear stochastic once one represents the initial conditions of the bath in a statistical way, with all of the
underlying dynamics being fully deterministic as per Zwanzig's original derivation of the non-relativistic Langevin equation. We fully agree that a more accurate approach would
involve a field-theoretical description, however, said observation does not invalidate the result
presented here: starting from a microscopic reversible Lagrangian we arrived at a fully relativistic Langevin equation. While
calling this the \emph{ultimate} form of the relativistic Langevin equation is problematic,
it is certainly \emph{a} relativistic Langevin equation, which for the first time covers a broad range of speeds up to fully relativistic speeds of both particle and bath oscillators. We have
made the statement of the paper as a whole more precise, hopefully
addressing the concerns of the Referee.

What follows is a detailed response to all the points raised by the Referees.

It is hoped that this extensively revised version of the manuscript is now ready for publication in JPhysA.

Sincerely\\
Aleksandr Petrosyan and Alessio Zaccone

\end{letterstyle}
\pagebreak

\textbf{Response to Referee 1}

\begin{Referee}
  (1) The structure of the manuscript and the writing style are to my
  opinion problematic. I got lost several times along the way, mostly
  because the authors do not clearly explain at the beginning what is
  their strategy and what they are going to do. For instance, a major
  part of the derivation is based on the solution that Mathematica
  finds, and then this starting point is progressively peeled off to
  obtain a more compact form for the solution. This is a perfectly
  fine method, but the reader discovers the intent of the authors as
  the text goes, and it is often hard to pick up the pieces. I would
  have liked a clear paragraph in the introduction, and at the
  beginning of sections III and IVwhich clearly explains what steps
  the authors are going to follow. As the manuscript is quite long and
  technical, this is to my opinion essential.

\end{Referee}

\begin{response}
    We thank the Referee for pointing this out. We have made the requested changes. In particular at the end of Secion~IV A we have added an outline of the entire process of solving the equation numerically.
\end{response}

\begin{Referee}
  \ldots Paragraph IV.C is in particular extremely hard to digest. While I
  fully understand that the full solution can not be displayed in the
  main text and has to be relegated to the supplemental material, the
  fact that there is only inline text in this section is not easy to
  follow. Some of the explanations should, I think, go straighter to
  the point, which might imply a bit more written math, while some of
  the very technical points could be relegated to the supplemental
  material. In the current state of the manuscript, it is for instance
  hard to grasp the very definition/meaning of some of the central
  quantities like $\bar{\xi}$, $\bar{\omega}$, $\hat{\xi}$ and $\hat{\omega}$
  (apart from being integration constants).
\end{Referee}

\begin{response}
    We have addressed the issue by extensively rewriting the Section~IV C. The Appendix D addresses the question as regards the integration constants.
\end{response}

\begin{Referee}
  2) There seems to be some confusion with the very foundation of the
  Langevin equation and its relation to the Caldeira-Leggett
  model. While this model is important because of its compactness and
  practicability in order to model a heat bath, the scope of the
  Langevin Equation is actually much wider than the CL model. In
  classical systems, the structure of the Langevin equation can
  actually be derived from first principles under very few assumptions
  (only that the microscopic dynamics is deterministic) using
  e.g. projection operator techniques such as the Mori-Zwanzig
  formalism. The Caldeira-Leggett model is ``just''  a case in which
  the dynamics of the bath can be decoupled from the one of the tagged
  particle. I find that the authors sometimes tend to overestimate the
  applicability of this model.

\end{Referee}

\begin{response}
  We thank the Referee for the observation. Indeed, the
  Caldeira-Legett model is one we have chosen because of the ease of
  extension (and yet the derivations and manipulations turned out to be non-trivial already for this simple model). In Section II we have added a paragraph, above Eq.~(1), to better clarify this point.
\end{response}

\begin{Referee}
  In addition, it should be stressed that the model is fully
  deterministic, while the ``true'' Langevin Equation is
  stochastic. The resulting equation of the Caldeira-Leggett can only
  be interpreted as effectively stochastic under some
  conditions. While these conditions are well-known in the classical
  case, I was not convinced by the discussion about this aspect in the
  relativistic case.  For instance, the validity of the stochastic
  nature of the equation in a classical process is often discussed in
  terms of timescale separation, i.e. the stochastic interpretation is
  valid if the main particle has a much slower dynamics that the one
  of the bath particles. This argument of timescale separation is much
  less straightforward to carry over in the relativistic case, which
  could be a nice aspect for the authors to discuss. In short, I think
  the authors should explain more why we can interpret their equation
  (27), which is intrinsically deterministic, as a stochastic
  process. In the current state of the manuscript, the reader might
  have the impression that the resulting ``Langevin'' equation is
  intrinsically stochastic.
\end{Referee}

\begin{response}
  We thank the Referee for the comment.
  The starting model is fully deterministic but the resulting generalized Langevin equation is fully stochastic inasmuch the initial conditions for the very large number of bath oscillators cannot be specified deterministically but are, instead, drawn from a statistical distribution (Boltzmann in the non-relativistic case, J\"{u}ttner in the relativistic case). Hence stochasticity of the final Langevin equation is a result of our ``ignorance'', as observers, about the initial conditions of a very large set of bath oscillators.

  This was all very nicely explained in Zwanzig's book ``Nonequilibrium statistical mechanics'' (Oxford Univ. Press, 2001), in Section 1.6 starting on page 21, and in his original seminal paper of 1973 where he introduced what is now known as Caldeira-Leggett model (in fact, a decade earlier than Caldeira and Leggett did), J. Stat. Phys. 9, 215–220 (1973),  https://doi.org/10.1007/BF01008729, in its classical version.

  So the reader is fully correct in having the impression that our generalized Langevin equation is fully stochastic. This is the beauty of Zwanzig's derivation of Langevin equation, as his method derives a dissipative non-time reversible stochastic Langevin equation from a deterministic reversible Hamiltonian of a particle coupled to a bath.

  Regarding the time-scale separation issue, as the Referee will convince herself/himself by reading Zwanzig's original paper or Section 1.6 in his book quoted above, there is no need, from a technical point of view, of assuming that the tagged particle is ``heavier'' than the bath oscillators, within this description (and in fact this assumption is never required in Zwanzig's original derivation of the non-relativistic case). This is because the fast degrees of freedom of the bath are effectively integrated out from the final dynamical equation by implementing the procedure outlined above.
  Furthermore, in our analysis we work at the level of the \emph{full} Langevin equation, and not just on the level of overdamped Brownian motion where indeed a larger time-scale separation between particle and bath is needed. As a matter of fact, we always retain the inertial terms in the dynamical equations and we never neglect them.

  To better clarify these points, in addition to discussing the correlation properties of the stochastic
  force, we have included a discussion concerning the Galilean relativistic generalized Langevin
  equation at the end of Section II. This shows how stochasticity emerges from Zwanzig's method of effectively ``hiding'' the fast degrees of freedom into the noise term, which is done by drawing the initial conditions of the bath oscillators from a statistical distribution. We also included a new subsection (VIII. A) dedicated to discussions of stochasticity right at the beginning of the "Discussion" section.
\end{response}

\begin{Referee}
  (1) Figure 1 is not easy to read as both curves superimpose. Maybe
  the authors could plot the difference between blue and orange in an
  inset?
\end{Referee}

\begin{response}
  We decided to remove Figure 1 in the revised manuscript, as it served no purpose and basically all the information of that figure is contained in the old Figure 2 (which is now the new Figure 1).
\end{response}

\begin{Referee}
  (2) where does the ansatz (23) come from ? I do not understand this
  particular shape
\end{Referee}

\begin{response}
  It is the lowest order term in the MacLaurin series expansion of the
  \(\xi(t, \mathbf{x})\) function, that has the requisite symmetries,
  particularly that of spatial inversion symmetry. Relevant discussion and clarification has been added to Section IV E.
\end{response}

\begin{Referee}
  (3) Can one explain the relation $\bar{\omega} = \gamma^{-3/4}$? Is it
  always verified when you change some parameters (e.g.~the initial
  conditions)?
\end{Referee}

\begin{response}
  This is a scaling relation that we obtained numerically, and was provided mainly to illustrate a point: that the shift in frequency could not be explained just by having extra Lorentz factors and a form of time dilation. We have clarified this point in the revised text in the penultimate paragraph of Section IV E, and we clarified that this scaling relation was found numerically for the conditions investigated here in the middle-bottom left column of page 6.
\end{response}

\begin{Referee}
  (4) The discussion part lacks a paragraph on the newly discovered
  term \(\mathbf{F}_{r}\). I think that this new term is one of the
  most important and exciting results of the manuscript, and I think
  it is not enough discussed.
\end{Referee}

\begin{response}
  The newly discovered term is discussed  in Section VII. Section VII B considers its behaviour in the non-relativistic limit \(F_r \rightarrow 0\), Section VII C discusses how its presence affects the symmetries of the generalized Langevin equation. Section VII E is concerned with how the presence of the restoring force may be reconciled with the fluctuation-dissipation relations.

  In our opinion a more complete exploration of the implications of the extra term would warrant another full-length paper, which we aim to prepare in due course. Also, the full discussion is closely related to the relativistic equivalents of the fluctuation-dissipation relations, which we have not yet been explored sufficiently.
\end{response}

\begin{Referee}
What about its statistical properties for instance?
\end{Referee}

\begin{response}
The contribution of the new force term to the statistical properties of the noise are discussed in Appendix E on page 19 of the revised version, and summarized in Eq. (E1).

Although we do discuss this important point, we find it appropriate to leave a more in-depth analysis of the statistical properties of the bath, together with an analysis of the thermalization kinetics predicted by the relativistic Langevin equation derived here, for future work.

This is also because the existence of this new force $\mathbf{F}_{r}$ is predicted on some specific form of the
relativistic correction parameter \(\xi(t, \mathbf{x})\), which we have only successfully evaluated numerically for a particular set of boundary and initial conditions.

We expect that having a
 broader discussion of the implications of the current results as
  a follow-up publication would provide us suitable time to
  investigate how general the behaviour of \(\mathbf{F}_{r}\) is under more different
  conditions, in order to be able to discuss its statistical properties in detail and more quantitatively.\\

\end{response}



\textbf{Response to Referee 2}

\begin{Referee}
\noindent 1- Title: It is precise and concise.\\
2- Abstract and conclusion:
- The abstract and conclusion should be detailed by adding more quantitative results and percentages of improvement, reduction …etc.
\end{Referee}

\begin{response}
Thank you for this valuable comment, we added more technical details about the derivation in both the abstract and the conclusion.
\end{response}

\begin{Referee}
\noindent 3- Introduction. It is well-written. However:
-         I suggest authors to re-examine it in terms of misspellings.
\end{Referee}

\begin{response}
We have revised the Introduction and corrected for misspellings.
\end{response}

\begin{Referee}
- In the last paragraph of the introduction, please clearly state the originality and novelty of the paper.
- You should re-structure the introduction and especially integrate references to new publications (2020, 2021,...).
\end{Referee}

\begin{response}
We addressed both these points in the revised version, see the last paragraph of the Introduction.
Also we have restructured the Introduction and introduced some references to papers appeared over the past years.
\end{response}

\begin{Referee}
\noindent 4- The numerical method used in the paper should be clearly described.
5- Quantitative and linear validation of the numerical algorithm is required.
\end{Referee}

\begin{response}
Thank you for this very useful comment.
We added more explanations of the numerical procedure in the revised version.
Importantly, we also performed an extensive quantitative validation of the numerical results by means of Bayesian inference. This is discussed in the new Section VI.
\end{response}

\begin{Referee}
6- The quality of the results is acceptable. The discussion section should be well presented and the results should be physically explained. The present explication is not enough.
\end{Referee}

\begin{response}
Thank you for the positive evaluation of our paper.
We made an effort in the new version of the manuscript to improve the presentation and to add more explanations, following your valuable suggestions as well as those of Referee 1.
\end{response}


\end{document}
